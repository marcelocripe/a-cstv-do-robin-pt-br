# aCSTV antiX Simple TV Starter ver. 1.08
# Copyright (C) 2021, 2022 The antiX community
# This file is distributed under the same license GPL V.3 as the aCSTV package.
# Robin 2021, 2022
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: aCSTV 1.08\n"
"Report-Msgid-Bugs-To: forum.antiXlinux.com\n"
"POT-Creation-Date: 2022-12-05 07:16+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: antiX-contribs (transifex.com)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: \n"

#. Base filename for aCSTV settings file.
#. Please consider the common conventions for file naming on antiX file
#. systems.
#: aCSTV-1.08.sh:36
msgid "aCSTV-Einstellungen"
msgstr ""

#. Base filename for aCSTV stations list file.
#. Please consider the common conventions for file naming on antiX file
#. systems.
#: aCSTV-1.08.sh:37
msgid "aCSTV-Sender"
msgstr ""

#. Base filename for aCSTV usage instructions file
#: aCSTV-1.08.sh:38
msgid "Bedienungsanleitung"
msgstr ""

#. translatable command line option. Please make sure to match the comand name
#. used in help text.
#: aCSTV-1.08.sh:56
msgid "--Hilfe"
msgstr ""

#. header of command line help message. max. length 80 characters for console.
#: aCSTV-1.08.sh:57
msgid "aCSTV — antiX Community Simple TV                             Ver."
msgstr ""

#. console help message text block, part 1.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:57
msgid ""
"Dieses Script bietet eine intuitive und einfach zu bedienende Benutzer-"
msgstr ""

#. console help message text block, part 2.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:58
msgid ""
"oberfläche  zur Wiedergabe  regionaler oder  international  verfügbarer"
msgstr ""

#. console help message text block, part 3.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:58
msgid ""
"Fernsehsender als Internet Live-Stream. Die Wiedergabe von Live-Streams"
msgstr ""

#. console help message text block, part 4.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:59
msgid ""
"setzt eine bestehende  Internetverbindung voraus.  Eine Mitschnitt- und"
msgstr ""

#. console help message text block, part 5.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:59
msgid ""
"eine  Szenenfotofunktion sowie Schnellzugriff auf eine Programmvorschau"
msgstr ""

#. console help message text block, part 6.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:60
msgid ""
"im Webbrowser ergänzen den Funktionsumfang. Die Wiedergabe erfolgt wahl-"
msgstr ""

#. console help message text block, part 7.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:60
msgid ""
"weise über den  primären PC-Bildschirm oder ein anderes an den Computer"
msgstr ""

#. console help message text block, part 8.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:61
msgid ""
"angeschlossenes  Anzeigegerät  wie z.B. ein  Fernseher.  Alle Parameter"
msgstr ""

#. console help message text block, part 9.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#: aCSTV-1.08.sh:61
msgid ""
"sind  frei konfigurierbar.  Weitere Informationen  in der Programmhilfe"
msgstr ""

#. console help message text block, part 10.
#. The text and the sentences can get freely spread over the following lines,
#. but keep in mind you have 80 characters per line max.
#. if you need even more lines in translation, add in this last line a »\n«
#. type line break followed by two blanks, then start with your text for an
#. 11. line and so on, as many lines you need.
#: aCSTV-1.08.sh:62
msgid "bei den »Einstellungen«."
msgstr ""

#. sub header for console help
#: aCSTV-1.08.sh:62
msgid "Befehlszeilenoptionen:"
msgstr ""

#. option description of console help. Add or remove as many dots from the
#. spacing as you need to meet the 80 character restriction.
#. Please make sure you translate the option »--Hilfe« to exactly
#. the same expression used for the command line option above.
#. Don't translate »-h«
#: aCSTV.sh:41
msgid "--Hilfe oder -h . . . . . . . . . . . . . . . Diese Kurzhilfe anzeigen."
msgstr ""

#. option description of console help. Add or remove as many dots from the
#. spacing as you need to meet the 80 character restriction.
#. Please make sure you translate the option »--Senderliste« to exactly
#. the same expression used for the command line option below.
#. Don't translate »-s«
#: aCSTV-1.08.sh:63
msgid ""
"--Senderliste oder -s <Datei> . . . . . Datei als Senderliste verwenden."
msgstr ""

#. console help text block, (followed by the web address of antiX forum).
#: aCSTV-1.08.sh:64
msgid "Fragen, Anregungen und Fehlermeldungen bitte an:"
msgstr ""

#. translatable command line option. Please make sure to match the comand name
#. used in help text.
#: aCSTV-1.08.sh:71
msgid "--Stationsliste"
msgstr ""

#. translatable window title. Please make sure your translation is accepted by
#. »xdotool« and »gtkdialog« as a valid window title.
#: aCSTV-1.08.sh:131 aCSTV-1.08.sh:178 aCSTV-1.08.sh:179 aCSTV-1.08.sh:479
#: aCSTV-1.08.sh:533 aCSTV-1.08.sh:751 aCSTV-1.08.sh:764 aCSTV-1.08.sh:849
#: aCSTV-1.08.sh:900 aCSTV-1.08.sh:933 aCSTV-1.08.sh:934 aCSTV-1.08.sh:1003
#: aCSTV-1.08.sh:1004 aCSTV-1.08.sh:1026 aCSTV-1.08.sh:1027 aCSTV-1.08.sh:1039
#: aCSTV-1.08.sh:1062 aCSTV-1.08.sh:1077 aCSTV-1.08.sh:1097 aCSTV-1.08.sh:1103
msgid "aCSTV antiX Community Simple TV Starter"
msgstr ""

#. Tray Icon tooltip and upper Window border text of dialog for editing
#. stations list
#: aCSTV-1.08.sh:136 aCSTV-1.08.sh:137 aCSTV-1.08.sh:138 aCSTV-1.08.sh:722
msgid "aCSTV Senderliste verwalten"
msgstr ""

#. Tray Icon tooltip and upper Window border text of dialog for modifying
#. settings
#: aCSTV-1.08.sh:141 aCSTV-1.08.sh:142 aCSTV-1.08.sh:143 aCSTV-1.08.sh:384
#: aCSTV-1.08.sh:505
msgid "aCSTV Einstellungen verwalten"
msgstr ""

#. header of an GUI error message dialog.
#: aCSTV-1.08.sh:251
msgid "aCSTV — Fehler"
msgstr ""

#. Error message in case the manual was not translated to the users UI
#. language yet.
#: aCSTV-1.08.sh:251
msgid ""
"Die Bedienungsanleitung für <i>aCSTV</i> wurde noch nicht in Ihre Sprache "
"übersetzt."
msgstr ""

#. Error message in case the manual was not translated to the users UI
#. language yet.
#: aCSTV-1.08.sh:251
msgid ""
"Bitte wählen Sie eine andere Sprache aus, in der die Programmhilfe angezeigt"
" werden wird."
msgstr ""

#. Name of language selection Pulldown menu
#: aCSTV-1.08.sh:251
msgid "Verfügbare Sprachen:"
msgstr ""

#. button engraving
#: aCSTV-1.08.sh:251
msgid "Auswahl akzeptieren"
msgstr ""

#. Base file name for recorded video files. Station name, recording date and
#. file type will be added by script along with all separators as needed.
#: aCSTV-1.08.sh:435
msgid "Sendungsmitschnitt"
msgstr ""

#. Base file name for taken photo files. Station name, recording date and file
#. type will be added by script, along with all separators as needed.
#: aCSTV-1.08.sh:440 aCSTV-1.08.sh:913 aCSTV-1.08.sh:1011 aCSTV-1.08.sh:1033
msgid "Szenenfoto"
msgstr ""

#. System tray title and header of a GUI dialog
#: aCSTV-1.08.sh:506
msgid "aCSTV Einstellungen"
msgstr ""

#. Text block of a GUI dialog. Part 1
#: aCSTV-1.08.sh:506
msgid "Bitte geben sie die gewünschten Voreinstellungen ein bzw. wählen"
msgstr ""

#. Text block of a GUI dialog. Part 2
#: aCSTV-1.08.sh:506
msgid "aus den Menüs die entsprechenden Optionen aus."
msgstr ""

#. Name of entry field in a GUI dialog. See pdf help text for its meaning.
#: aCSTV-1.08.sh:511
msgid "Max. Videobitrate (in kbps):"
msgstr ""

#. Name of entry field in a GUI dialog. Refers to the screen number to be used
#. for displaying TV, e.g. 0 or 1
#: aCSTV-1.08.sh:512
msgid "Ausgabegerät (Bildschirm):"
msgstr ""

#. Name of entry field in a GUI dialog. Referring to the URL pointing to the
#. local program information web site.
#: aCSTV-1.08.sh:513
msgid "URL für Programminformationen:"
msgstr ""

#. Checkbox in a GUI Dialog, refers to initial display mode of TV on screen.
#: aCSTV-1.08.sh:514
msgid "Starte Wiedergabe mit Vollbild"
msgstr ""

#. Name of entry field in a GUI dialog. Refers to the directory where aCSTV
#. will store any screenshots.
#: aCSTV-1.08.sh:515
msgid "Verzeichnis für Szenenfotos:"
msgstr ""

#. Name of entry field in a GUI dialog. Refers to the file format of any
#. screenshots (e.g.png or jpeg)
#: aCSTV-1.08.sh:516
msgid "Ausgabeformat für Szenenfotos:"
msgstr ""

#. Name of entry field in a GUI dialog. Referring to the directiory to be used
#. by aCSTV for storing recordings of the live TV stream.
#: aCSTV-1.08.sh:517
msgid "Verzeichnis für Sendungsmitschnitte:"
msgstr ""

#. Checkbox in a GUI Dialog, refers to initial display mode of TV on screen.
#: aCSTV-1.08.sh:518
msgid "Wiedergabe im Vordergrund"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:519
msgid "Aktualisieren"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:519
msgid "Programmhilfe"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:519
msgid "Senderliste bearbeiten"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:519 aCSTV-1.08.sh:1094
msgid "Abbruch"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:519
msgid "Speichern"
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:599
msgid "Exzessive Senderliste."
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:599
msgid ""
"Die Verarbeitung von Senderlisten mit mehr als 50-60 aktiven\\nEinträgen "
"kann die Reaktionszeit von aCSTV erheblich verlangsamen.\\n"
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:599
msgid ""
"Bitte erwägen Sie, Ihre Senderliste zugunsten einer beschleunigten"
"\\nReaktion in verschiedenen aCSTV Funktionen zu beschränken. "
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:599
msgid ""
"Dies\\nbetrifft u.a. den Programmstart und die Bearbeitung von Senderlisten."
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:599
msgid "Bitte gedulden Sie sich, bis die Verarbeitung abgeschlossen ist."
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:627
msgid "Warnung"
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:628
msgid "Zu viele Sender oder zu lange Sendernamen."
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:628
msgid ""
"aCSTV kann maximal 264 Stationswahltasten à 13 Zeichen bereitstellen. "
"Kürzere Sendernamen\\nerlauben mehr Tasten. "
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:628
msgid ""
"Bitte reduzieren Sie die Anzahl aktiver Sender in Ihrer Senderliste durch"
"\\nAuskommentieren oder verkürzen die Sendernamen. "
msgstr ""

#. Informational GUI message
#: aCSTV-1.08.sh:628
msgid ""
"Setzen Sie dazu in den <i>Einstellungen</i> unter\\n<i>Senderliste "
"bearbeiten</i> ein Rautezeichen (#) an den Beginn derjenigen Stationsnamen, "
"die Sie\\ndeaktivieren möchten oder nutzen Akronyme:"
msgstr ""

#. Informational GUI message (Example)
#: aCSTV-1.08.sh:628
msgid "Sendername"
msgstr ""

#. Example string in GUI dialog window
#: aCSTV-1.08.sh:628
msgid "Zweites-Deutsches-Fernsehen"
msgstr ""

#. Example string in GUI dialog window
#: aCSTV-1.08.sh:628
msgid "ZDF"
msgstr ""

#. Example string in GUI dialog window
#: aCSTV-1.08.sh:628
msgid "Saarländischer-Rundfunk"
msgstr ""

#. Example string in GUI dialog window
#: aCSTV-1.08.sh:628
msgid "SR"
msgstr ""

#. Informational GUI message
#. The two tags <u> and </u> means underlined Text. Make sure to keep these untouched.
#: aCSTV-1.08.sh:628
msgid ""
"Es werden <u>nur so viele Sender aus der Liste verwendet, wie das Tastenfeld "
"aufnehmen kann</u>, alle\\nweiteren Einträge werden ignoriert. Die Summe der "
"Buchstaben aller Sendernamen im Tastenfeld\\nzusammengenommen ist daher auf "
"ca. 3400 begrenzt."
msgstr ""

#. Part 1 of a sentence
#. Complete sentence reading: „(Verwende die ersten 267 von 3683 aktiven Einträgen.)“
#: aCSTV-1.08.sh:628
msgid "(Verwende die ersten"
msgstr ""

#. Part 2 of a sentence
#: aCSTV-1.08.sh:628
msgid "von"
msgstr ""

#. Part 3 of a sentence
#: aCSTV-1.08.sh:628
msgid "aktiven Einträgen.)"
msgstr ""

#. Header of a GUI dialog
#: aCSTV-1.08.sh:723
msgid "aCSTV Senderliste"
msgstr ""

#. Text block of a GUI dialog. Part 1
#: aCSTV-1.08.sh:723
msgid "Alle Einträge lassen sich per Doppelklick bearbeiten."
msgstr ""

#. Text block of a GUI dialog. Part 2
#. The words between the italic tags <i>...</i> must point to the translation
#. of the respective button engraving above.
#: aCSTV-1.08.sh:723
msgid ""
"Sollen Einträge entfernt werden, diese mit einem Häkchen markieren und die "
"Taste <i>»Einträge Entfernen«</i> drücken."
msgstr ""

#. Text block of a GUI dialog. Part 3
#. The words between the italic tags <i>...</i> must point to the translation
#. of the respective button engraving above.
#: aCSTV-1.08.sh:723
msgid ""
"Neue Einträge mit der Taste <i>»Neuen Eintrag hinzufügen«</i> anlegen und "
"anschließend bearbeiten."
msgstr ""

#. Header entry of an list field referring to the consecutive numbers of the
#. list entries.
#: aCSTV-1.08.sh:728
msgid "Nr."
msgstr ""

#. Header entry of an list field referring to the Names of the TV stations
#. listed.
#: aCSTV-1.08.sh:728
msgid "Sender"
msgstr ""

#. Header entry of an list field referring to the URLs of the respective live
#. stream address of the TV stations listed.
#: aCSTV-1.08.sh:728
msgid "Adresse (URL)"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:730
msgid "Markierte Einträge löschen"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:731
msgid "Neuen Eintrag hinzufügen"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:732
msgid "Änderungen verwerfen"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:733
msgid "Senderliste speichern"
msgstr ""

#: aCSTV-1.08.sh:788 aCSTV-1.08.sh:789
msgid "aCSTV Senderliste aktualisieren"
msgstr ""

#: aCSTV-1.08.sh:788
msgid "Sender prüfen"
msgstr ""

#: aCSTV-1.08.sh:789
msgid "Verbindungsaufbau zum Sender wird geprüft:"
msgstr ""

#: aCSTV-1.08.sh:790 aCSTV-1.08.sh:803
msgid "Abbrechen"
msgstr ""

#: aCSTV-1.08.sh:798
msgid "Stationsliste aktualisieren"
msgstr ""

#: aCSTV-1.08.sh:799
msgid "aCSTV Stationsliste aktualisieren"
msgstr ""

#: aCSTV-1.08.sh:799
msgid "Bitte warten Sie, bis die Aktualisierung abgeschlossen ist."
msgstr ""

#: aCSTV-1.08.sh:801
msgid "Länderlisten:"
msgstr ""

#: aCSTV-1.08.sh:802
msgid "Prüfe auf Verfügbarkeit"
msgstr ""

#: aCSTV-1.08.sh:803
msgid "Aktualisierung starten"
msgstr ""

#: aCSTV-1.08.sh:837
msgid "Prüfe"
msgstr ""

#. header of an GUI error message dialog.
#: aCSTV-1.08.sh:882
msgid "Verbindungsfehler"
msgstr ""

#. text block of an GUI error message dialog.
#: aCSTV-1.08.sh:883
msgid "Keine Internetverbindung gefunden."
msgstr ""

#. text block of an GUI error message dialog. Part 1
#: aCSTV-1.08.sh:883
msgid "Stellen Sie bitte eine Internetverbindung her, bevor"
msgstr ""

#. text block of an GUI error message dialog. Part 2
#: aCSTV-1.08.sh:883
msgid "Sie es erneut versuchen, oder Sie können aCSTV beenden."
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:884
msgid "aCSTV beenden"
msgstr ""

#. Button engraving
#: aCSTV-1.08.sh:884
msgid "Erneut versuchen"
msgstr ""

#. Header of main dialog with all station buttons.
#: aCSTV-1.08.sh:902
msgid "TV-Sender"
msgstr ""

#. Button tool tip, will be followed by station name
#: aCSTV-1.08.sh:908
msgid "Ein- oder Umschalten zu Sender"
msgstr ""

#. Button tool tip
#: aCSTV-1.08.sh:945
msgid "Aktuelle Programmvorschau im Browser zeigen"
msgstr ""

#. Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV-1.08.sh:946
msgid " Programmvorschau "
msgstr ""

#. Button tool tip
#: aCSTV-1.08.sh:949
msgid "aCSTV Grundeinstellungen vornehmen"
msgstr ""

#. Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV-1.08.sh:950
msgid " Einstellungen "
msgstr ""

#. Button tool tip
#: aCSTV-1.08.sh:954
msgid "Laufendes Programm ausschalten"
msgstr ""

#. Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV-1.08.sh:955
msgid " Stop "
msgstr ""

#. Button tool tip
#: aCSTV-1.08.sh:973
msgid "aCSTV verlassen"
msgstr ""

#. Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV-1.08.sh:974
msgid " Beenden "
msgstr ""

#. Button tool tip
#: aCSTV-1.08.sh:982
msgid "Senderliste wechseln"
msgstr ""

#. Button tool tip
#: aCSTV-1.08.sh:987
msgid "Videomitschnitt der aktuellen Filmszene anfertigen"
msgstr ""

#. Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV-1.08.sh:988
msgid " Videoaufnahme "
msgstr ""

#. Button tool tip
#: aCSTV-1.08.sh:1006
msgid "Laufenden Videomitschnitt stoppen"
msgstr ""

#. Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV-1.08.sh:1007
msgid " Aufnahme Stop "
msgstr ""

#. Button tool tip
#: aCSTV-1.08.sh:1029
msgid "Standbild der aktuellen Filmszene aufnehmen"
msgstr ""

#. Button engraving. Swallow the blanks if your translation needs more space.
#: aCSTV-1.08.sh:1030
msgid " Szenenfoto "
msgstr ""

#: aCSTV-1.08.sh:1088 aCSTV-1.08.sh:1089
msgid "aCSTV Senderliste wechseln"
msgstr ""

#: aCSTV-1.08.sh:1089
msgid "Bitte wählen Sie die gewünschte Senderliste aus dem Menü."
msgstr ""

#: aCSTV-1.08.sh:1093
msgid "Senderliste:"
msgstr ""

#: aCSTV-1.08.sh:1094
msgid "Liste wechseln"
msgstr ""

#: aCSTV-1.08.sh:1119 aCSTV-1.08.sh:1199 aCSTV-1.08.sh:1200 aCSTV-1.08.sh:1204
msgid "aCSTV Aufnahme"
msgstr ""

#: aCSTV-1.08.sh:1160
msgid "Jahren"
msgstr ""

#: aCSTV-1.08.sh:1162
msgid "Monaten"
msgstr ""

#: aCSTV-1.08.sh:1164
msgid "Tagen"
msgstr ""

#: aCSTV-1.08.sh:1166
msgid "Std."
msgstr ""

#: aCSTV-1.08.sh:1168
msgid "Min."
msgstr ""

#: aCSTV-1.08.sh:1170 aCSTV-1.08.sh:1178 aCSTV-1.08.sh:1180
msgid "Sek."
msgstr ""

#: aCSTV-1.08.sh:1200
msgid "Der Mitschnitt der laufenden Sendung wird in der Datei"
msgstr ""

#: aCSTV-1.08.sh:1200
msgid "im Verzeichnis"
msgstr ""

#: aCSTV-1.08.sh:1200
msgid "gespeichert. "
msgstr ""

#: aCSTV-1.08.sh:1200
msgid "Gegenwärtige Dateigröße:"
msgstr ""

#: aCSTV-1.08.sh:1200
msgid "Der verfügbare Speicherplatz von"
msgstr ""

#: aCSTV-1.08.sh:1200
msgid "reicht bei der\\n\\taktuellen durchschnittlichen Datenrate von"
msgstr ""

#: aCSTV-1.08.sh:1200
msgid "näherungsweise für eine Aufzeichnung von"
msgstr ""

#: aCSTV-1.08.sh:1200
msgid ""
"Zum Beenden der Aufnahme drücken Sie bitte im\\n\\taCSTV Hauptfenster die "
"Taste <i>»Aufnahme Stop«</i>."
msgstr ""
